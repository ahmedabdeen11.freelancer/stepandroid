package com.dell.step.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dell.step.R;
import com.dell.step.models.Group;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class GroupsAdapter extends RecyclerView.Adapter<GroupsAdapter.ViewHolder> {

    public List<Group> groupList;
    public Context context;


    public GroupsAdapter(List<Group> groupList){

        this.groupList = groupList;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_class_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final String groupName = groupList.get(position).getName();
        holder.setGroupName(groupName);

    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public View view;

        public TextView classView;


        public ViewHolder(final View itemView) {
            super(itemView);

            view = itemView;

        }

        public void setGroupName(String groupName){

            classView = view.findViewById(R.id.class_name);
            classView.setText(groupName);


        }
    }

}
