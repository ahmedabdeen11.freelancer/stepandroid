package com.dell.step.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dell.step.R;
import com.dell.step.activities.QuestionDetailsActivity;
import com.dell.step.models.Question;
import com.dell.step.models.Reply;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.ViewHolder> {

    public List<Reply> replyList;
    public Context context;
    DateFormat dateFormat;

    public ReplyAdapter(Context context, List<Reply> replyList) {
        this.context = context;
        this.replyList = replyList;

        dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_reply_item, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {

        final Reply reply = replyList.get(position);

        holder.reply_description.setText(reply.getReply());
        holder.user_name.setText(reply.getUsername());

        if(reply.getTimeStamp() == null){
            holder.replyDate.setText("Just now");
        }else{
            holder.replyDate.setText(dateFormat.format(reply.getTimeStamp()));
        }
    }

    @Override
    public int getItemCount() {
        return replyList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView reply_description;

        private TextView user_name;
        private TextView replyDate;

        public ViewHolder(View itemView) {
            super(itemView);

            reply_description = itemView.findViewById(R.id.reply_text);
            user_name = itemView.findViewById(R.id.reply_user_name);
            replyDate = itemView.findViewById(R.id.reply_date);
        }

    }


}
