package com.dell.step.adapters;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.activities.RegisterActivity;
import com.dell.step.app.Helper;
import com.dell.step.models.Resource;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


public class ResourcesAdapter extends RecyclerView.Adapter<ResourcesAdapter.ViewHolder> {

    public List<Resource> resourceList;
    public Context context;


    public ResourcesAdapter(Context context, List<Resource> resourceList){

        this.context = context;
        this.resourceList = resourceList;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_class_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        final String resourceTitle = resourceList.get(position).getTitle();
        final String resourceUrl = resourceList.get(position).getUrl();
        holder.setResourceTitle(resourceTitle);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                            , 2);

                }
                else{
                    DownloadManager.Request req=new DownloadManager.Request(Uri.parse(resourceUrl));

                    req.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
                            | DownloadManager.Request.NETWORK_MOBILE)
                            .setAllowedOverRoaming(false)
                            .setTitle(resourceTitle)
                            .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                                    "");
                    DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                    downloadManager.enqueue(req);

                    Toast.makeText(context, "Downloading...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return resourceList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public View view;

        public TextView classView;


        public ViewHolder(final View itemView) {
            super(itemView);

            view = itemView;

        }

        public void setResourceTitle(String resourceTitle){

            classView = view.findViewById(R.id.class_name);
            classView.setText(resourceTitle);


        }
    }

}
