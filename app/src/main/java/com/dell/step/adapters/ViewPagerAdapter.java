package com.dell.step.adapters;


import com.dell.step.fragments.MembersFragment;
import com.dell.step.fragments.QuestionsFragment;
import com.dell.step.fragments.ResourcesFragment;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private ResourcesFragment resourcesFragment;
    private QuestionsFragment questionsFragment;
    private MembersFragment membersFragment;


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {



        switch (position){

            case 0:
                resourcesFragment = new ResourcesFragment();
                return resourcesFragment;


            case 1:
                questionsFragment = new QuestionsFragment();
                return questionsFragment;


            case 2:
                membersFragment = new MembersFragment();
                return membersFragment;

            default:
                return null;

        }

    }

    public MembersFragment getMembersFragment(){
        return membersFragment;
    }

    public QuestionsFragment getQuestionsFragment(){
        return questionsFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    public CharSequence getPageTitle(int position){

        switch (position){

            case 0:

                return "Resources" ;


            case 1:

                return "Questions" ;


            case 2:

                return "Members" ;

            default:
                return null;

        }


    }
}
