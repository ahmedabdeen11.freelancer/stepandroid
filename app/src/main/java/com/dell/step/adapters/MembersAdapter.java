package com.dell.step.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dell.step.R;
import com.dell.step.activities.ClassDetailsActivity;
import com.dell.step.models.Member;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.ViewHolder> {

    private static final String TAG = "tag" ;
    public List<Member> memberList;
    public Context context;
    public FirebaseFirestore firebaseFirestore;
    public FirebaseAuth mauth;


    public MembersAdapter(Context context, List<Member> memberList) {
        this.context = context;
        this.memberList = memberList;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_class_item, parent, false);
        context = parent.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        mauth = FirebaseAuth.getInstance();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final String member_email = memberList.get(position).getEmail();
        holder.setMember(member_email);

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                CharSequence[] options;
                options = new CharSequence[1];
                options[0] = "Remove";

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle(member_email);
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which){
                            case 0:{
                                ((ClassDetailsActivity)context).deleteMember(memberList.get(position).getUid());
                            }
                            break;

                            default: //Do nothing
                        }


                    }
                });
                builder.show();


                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return memberList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private View mVIew;

        private TextView memberEmailView;

        private TextView user_name;

        public ViewHolder(View itemView) {
            super(itemView);

            mVIew = itemView;
        }

        public void setMember(String memberEmail) {

            memberEmailView = mVIew.findViewById(R.id.class_name);
            memberEmailView.setText(memberEmail);

        }


    }



}
