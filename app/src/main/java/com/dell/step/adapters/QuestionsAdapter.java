package com.dell.step.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dell.step.R;
import com.dell.step.activities.ClassDetailsActivity;
import com.dell.step.activities.QuestionDetailsActivity;
import com.dell.step.app.Helper;
import com.dell.step.models.Question;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder> {

    public List<Question> questionList;
    public Context context;
    public FirebaseFirestore firebaseFirestore;
    DateFormat dateFormat;


    public QuestionsAdapter(Context context, List<Question> questionList) {
        this.context = context;
        this.questionList = questionList;
        dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_question_item, parent, false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final Question question = questionList.get(position);

        holder.question_description.setText(question.getQuestion());
        holder.user_name.setText(question.getUserName());

        if(question.getTimeStamp() == null){
            holder.questionDate.setText("Just now");
        }else{
            holder.questionDate.setText(dateFormat.format(question.getTimeStamp()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, QuestionDetailsActivity.class);
                intent.putExtra("question", question);
                context.startActivity(intent);
            }
        });


        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                CharSequence[] options;
                options = new CharSequence[1];
                options[0] = "Remove";

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setTitle("");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which){
                            case 0:{
                                ((ClassDetailsActivity)context).deleteQuestion(questionList.get(position).getUid());
                            }
                            break;

                            default: //Do nothing
                        }


                    }
                });
                builder.show();


                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView question_description;

        private TextView user_name;

        private TextView questionDate;

        public ViewHolder(View itemView) {
            super(itemView);

            question_description = itemView.findViewById(R.id.question_text);
            user_name = itemView.findViewById(R.id.question_user_name);
            questionDate = itemView.findViewById(R.id.question_post_date);
        }

    }


}
