package com.dell.step.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.activities.ClassDetailsActivity;
import com.dell.step.models.Class;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.ViewHolder> {

    public List<Class> classList;
    public Context context;


    public ClassesAdapter(Context context, List<Class> classList){
        this.context = context;
        this.classList = classList;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_class_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        final String className = classList.get(position).getName();
        holder.setClassText(className);

        String classSession = classList.get(position).getSession();
        holder.setClassSession(classSession);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ClassDetailsActivity.class);
                intent.putExtra("class" , classList.get(position));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return classList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public View view;

        public TextView classView , sessionView;


        public ViewHolder(final View itemView) {
            super(itemView);

            view = itemView;

        }

        public void setClassText(String class_text){

            classView = view.findViewById(R.id.class_name);
            classView.setText(class_text);


        }

        public  void setClassSession (String class_session_text){

            sessionView = view.findViewById(R.id.class_session);
            sessionView.setText(class_session_text);
        }
    }

}
