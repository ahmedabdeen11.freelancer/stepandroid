package com.dell.step.models;

import java.io.Serializable;

public class Resource implements Serializable {

    private String uid;
    private String title;
    private String url;

    public Resource() {
    }

    public Resource(String uid, String title, String url) {
        this.uid = uid;
        this.title = title;
        this.url = url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
