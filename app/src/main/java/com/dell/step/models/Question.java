package com.dell.step.models;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;

import java.io.Serializable;
import java.util.Date;

public class Question implements Serializable {

    private String uid;
    private String userName;
    @ServerTimestamp
    private Date timeStamp;
    private String question;
    private String questionOwnerUid;

    public Question() {
    }

    public Question(String questionOwnerUid, String userName, String question) {
        this.questionOwnerUid = questionOwnerUid;
        this.userName = userName;
        this.question = question;
    }

    @Exclude
    public String getUid() {
        return uid;
    }

    @Exclude
    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionOwnerUid() {
        return questionOwnerUid;
    }

    public void setQuestionOwnerUid(String questionOwnerUid) {
        this.questionOwnerUid = questionOwnerUid;
    }
}
