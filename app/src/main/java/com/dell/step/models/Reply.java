package com.dell.step.models;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class Reply {
    private String uid;
    private String username;
    private String reply;
    @ServerTimestamp
    private Date timeStamp;

    public Reply() {
    }

    public Reply(String uid, String username, String reply) {
        this.uid = uid;
        this.username = username;
        this.reply = reply;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
