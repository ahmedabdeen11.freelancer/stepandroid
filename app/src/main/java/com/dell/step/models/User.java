package com.dell.step.models;

import java.io.Serializable;

public class User implements Serializable {

    private String profileImg;
    private String username;
    private String email;
    private String type;

    public User() {
    }

    public User(String profileImg, String username, String email, String type) {
        this.profileImg = profileImg;
        this.username = username;
        this.email = email;
        this.type = type;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public void setProfileImg(String profileImg) {
        this.profileImg = profileImg;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
