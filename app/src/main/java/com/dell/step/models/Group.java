package com.dell.step.models;

import java.io.Serializable;

public class Group implements Serializable {

    private String name;
    private String instructorUid;

    public Group() {
    }

    public Group(String name, String instructorUid) {
        this.name = name;
        this.instructorUid = instructorUid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstructorUid() {
        return instructorUid;
    }

    public void setInstructorUid(String instructorUid) {
        this.instructorUid = instructorUid;
    }
}
