package com.dell.step.models;

import com.google.firebase.firestore.Exclude;

import java.io.Serializable;

public class Class implements Serializable {

    private String uid;
    private String name;
    private String session;
    private String instructorUid;

    public Class() {
    }

    public Class(String name, String session, String instructorUid) {
        this.name = name;
        this.session = session;
        this.instructorUid = instructorUid;
    }

    @Exclude
    public String getUid() {
        return uid;
    }

    @Exclude
    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getInstructorUid() {
        return instructorUid;
    }

    public void setInstructorUid(String instructorUid) {
        this.instructorUid = instructorUid;
    }
}
