package com.dell.step.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.dell.step.R;
import com.dell.step.fragments.ClassesFragment;
import com.dell.step.fragments.GroupsFragment;
import com.dell.step.models.User;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.view.View;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Views
    Toolbar toolbar;
    SpinKitView progress;
    FrameLayout progressBarLayout;
    NavigationView navigationView;

    //Firebase
    FirebaseAuth mAuth;
    CollectionReference usersCollection;

    //Current User
    public static User currentUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initToolbar();
        initProgressBar();
//        initDrawer();
//        initNavigation();
        initFirebase();
        getCurrentUserData();
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        usersCollection = FirebaseFirestore.getInstance().collection(getString(R.string.collection_users));
    }

    private void getCurrentUserData(){
        String uid = mAuth.getCurrentUser().getUid();
        usersCollection.document(uid).addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){
                    currentUser = documentSnapshot.toObject(User.class);
                    progressBarLayout.setVisibility(View.GONE);

                    /*View headerView = navigationView.getHeaderView(0);
                    ImageView profileImg = headerView.findViewById(R.id.userImg);
                    TextView username = headerView.findViewById(R.id.userName);
                    TextView userRole = headerView.findViewById(R.id.userRole);

                    Glide.with(MainActivity.this)
                            .load(currentUser.getProfileImg())
                            .into(profileImg);

                    username.setText(currentUser.getUsername());
                    userRole.setText(currentUser.getType());*/
                    inflateClassesFragment();

                }else{
                    Toast.makeText(MainActivity.this, "User not found!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void initViews(){
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Classes");
    }

    private void initProgressBar(){
        Circle circle = new Circle();
        progress.setIndeterminateDrawable(circle);
    }

    /*private void initDrawer(){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNavigation(){
        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }*/

    private void inflateClassesFragment(){
        ClassesFragment fragment = new ClassesFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.content_fragment, fragment, getString(R.string.classes_fragment));
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    //This is for menu bar we created in  Menu Resource by Inflate Method
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //When a Menu is selected from menu

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.action_logout) {

            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);

            builder.setMessage("Are you sure you want to logout?");
            builder.setCancelable(true);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();

                    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            android.app.AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }

        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_classes) {
            ClassesFragment fragment = new ClassesFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_fragment, fragment, getString(R.string.classes_fragment));
            transaction.commit();
            getSupportActionBar().setTitle("Classes");
        } else if (id == R.id.nav_groups) {
            GroupsFragment fragment = new GroupsFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.content_fragment, fragment, getString(R.string.groups_fragment));
            transaction.commit();
            getSupportActionBar().setTitle("Groups");
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Loading please wait...", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK){
            ClassesFragment classesFragment = (ClassesFragment) getSupportFragmentManager().findFragmentByTag(getString(R.string.classes_fragment));
            if(classesFragment != null)
                classesFragment.getClasses();
        }

    }
}
