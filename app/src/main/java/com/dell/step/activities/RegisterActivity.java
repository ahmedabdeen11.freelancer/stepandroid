package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import id.zelory.compressor.Compressor;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dell.step.R;
import com.dell.step.app.FileUtil;
import com.dell.step.app.Helper;
import com.dell.step.models.User;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.rpc.Help;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = RegisterActivity.class.getSimpleName();
    private final Context context = RegisterActivity.this;

    //Views
    EditText etUsername;
    EditText etEmail;
    EditText etPassword;
    EditText etConfirmPassword;
    AppCompatSpinner typeSpinner;
    FrameLayout imageOptionsLayout;
    ImageView profileImg;
    SpinKitView progress;
    FrameLayout progressBarLayout;

    //Adapter
    private ArrayAdapter<String> adapter;

    //Variables
    private final int PERMISSIONS_REQUEST_STORAGE_ProfileImg = 1;
    private final int REQUEST_TAKE_GALLERY_image = 1;
    private final int REQUEST_TAKE_Camera_image = 2;
    private Uri profileImgUri;

    //Firebase
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private CollectionReference usersCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initViews();
        setupTypeSpinner();
        initProgressBar();
        initFirebase();
    }

    private void initViews(){
        etUsername = findViewById(R.id.editUsernameSign);
        etEmail = findViewById(R.id.editEmailSign);
        etPassword = findViewById(R.id.edtPasswordSign);
        etConfirmPassword = findViewById(R.id.edtPassword2Sign);
        typeSpinner = findViewById(R.id.typeSpinner);
        imageOptionsLayout = findViewById(R.id.image_options_layout);
        profileImg = findViewById(R.id.profileImg);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);
    }

    private void initProgressBar(){
        Circle circle = new Circle();
        progress.setIndeterminateDrawable(circle);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        usersCollection = FirebaseFirestore.getInstance().collection(getString(R.string.collection_users));
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(mAuth.getCurrentUser() != null){
                    uploadProfileImageToFirebase();
                }
            }
        };
    }

    private void setupTypeSpinner(){
        List<String> dataset = new LinkedList<>(Arrays.asList("Teacher", "Student"));
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, dataset);
        adapter.setDropDownViewResource(R.layout.layout_spinner);
        typeSpinner.setAdapter(adapter);
    }

    public void login(View view) {
        finish();
    }

    private boolean validateForm() {
        boolean valid = true;

        String mUsername = etUsername.getText().toString();
        if(TextUtils.isEmpty(mUsername)){
            etUsername.setError(getString(R.string.error_required));
            valid = false;
        } else {
            etUsername.setError(null);
        }

        String mEmail = etEmail.getText().toString();
        if(TextUtils.isEmpty(mEmail)){
            etEmail.setError(getString(R.string.error_required));
            valid = false;
        }else {
            etEmail.setError(null);
        }

        String mPassword = etPassword.getText().toString();
        if(TextUtils.isEmpty(mPassword)){
            etPassword.setError(getString(R.string.error_required));
            valid = false;
        }else {
            etPassword.setError(null);
        }

        String mConfirmPassword = etConfirmPassword.getText().toString();
        if(TextUtils.isEmpty(mConfirmPassword)){
            etConfirmPassword.setError(getString(R.string.error_required));
            valid = false;
        }else {
            etConfirmPassword.setError(null);
        }

        return valid;
    }

    public void pickImgFromCamera(){
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, REQUEST_TAKE_Camera_image);
    }

    public void pickImgFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent,"Select Image"), REQUEST_TAKE_GALLERY_image);
    }

    public void openCamera(View view) {
        pickImgFromCamera();
        imageOptionsLayout.setVisibility(View.GONE);
    }

    public void openGallery(View view) {
        pickImgFromGallery();
        imageOptionsLayout.setVisibility(View.GONE);
    }

    public void disableImageOptions(View view) {
        imageOptionsLayout.setVisibility(View.GONE);
    }

    public void pickProfileImage(View view) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                    , PERMISSIONS_REQUEST_STORAGE_ProfileImg);

        }
        else{
            Helper.hideKeyboard(RegisterActivity.this);
//            imageOptionsLayout.setVisibility(View.VISIBLE);
            pickImgFromGallery();
        }
    }

    public void register(View view) {
        if (validateForm()){
            if(profileImgUri == null){
                Toast.makeText(context, "Please pick profile image", Toast.LENGTH_SHORT).show();
            }else if(!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())){
                Toast.makeText(context, "Password doesn't match...", Toast.LENGTH_SHORT).show();
            }else{
                //Register
                progressBarLayout.setVisibility(View.VISIBLE);
                registerUserToFirebase();
            }
        }
    }

    private void registerUserToFirebase(){
        Helper.hideKeyboard(RegisterActivity.this);
        if(validateForm()){
            progressBarLayout.setVisibility(View.VISIBLE);
            mAuth.createUserWithEmailAndPassword(etEmail.getText().toString(),
                    etPassword.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                progressBarLayout.setVisibility(View.GONE);
                                try {
                                    throw task.getException();
                                } catch(FirebaseAuthWeakPasswordException e) {
                                    etPassword.setError(getResources().getString(R.string.error_password_is_incomplete));
                                } catch(FirebaseAuthInvalidCredentialsException e) {
                                    etEmail.setError(getResources().getString(R.string.error_invalid_email));
                                } catch(FirebaseAuthUserCollisionException e) {
                                    etEmail.setError(getResources().getString(R.string.error_email_exists));

                                } catch(Exception e) {
                                    Toast.makeText(context, getResources().getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    });
        }
    }

    private void uploadProfileImageToFirebase(){
        final StorageReference ref = FirebaseStorage.getInstance().getReference()
                .child(getString(R.string.storage_profile_imgs))
                .child(mAuth.getCurrentUser().getUid());

        File compressedImageFile = null;
        try {
            File projectImgFile = FileUtil.from(this, profileImgUri);
            compressedImageFile = new Compressor(this).compressToFile(projectImgFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        UploadTask uploadTask = ref.putFile(Uri.fromFile(compressedImageFile));

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String downloadURL = downloadUri.toString();
                    saveProfileDataToFirestore(downloadURL);
                }
                else {
                    progressBarLayout.setVisibility(View.GONE);
                    Toast.makeText(context, getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveProfileDataToFirestore(String profileImgUrl){
        User user = new User(
                profileImgUrl,
                etUsername.getText().toString(),
                etEmail.getText().toString(),
                typeSpinner.getSelectedItemPosition() == 0? "Teacher" : "Student"
                );

        usersCollection.document(mAuth.getCurrentUser().getUid()).set(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            Toast.makeText(context, "Register Success", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(context, "Failed to create profile please try again later...", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Loading please wait...", Toast.LENGTH_SHORT).show();
    }

    //-----Override Methods------//
    @Override
    protected void onResume() {
        super.onResume();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_TAKE_GALLERY_image: {
                    profileImgUri = data.getData();
                    Glide.with(context)
                            .load(profileImgUri)
                            .into(profileImg);
                }
                break;

                case REQUEST_TAKE_Camera_image: {
                    profileImgUri = data.getData();
                    Glide.with(context)
                            .load(profileImgUri)
                            .into(profileImg);
                }
                break;

                default:  //Do nothing
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case PERMISSIONS_REQUEST_STORAGE_ProfileImg:

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, getString(R.string.error_permission_denied), Toast.LENGTH_SHORT).show();
                }
                else{
//                    imageOptionsLayout.setVisibility(View.VISIBLE);
                    pickImgFromGallery();
                }
                break;
        }
    }
}
