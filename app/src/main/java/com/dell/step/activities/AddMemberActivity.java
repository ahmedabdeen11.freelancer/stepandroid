package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.models.Member;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import javax.annotation.Nullable;

public class AddMemberActivity extends AppCompatActivity {

    //Views
    Toolbar toolbar;
    SpinKitView progress;
    FrameLayout progressBarLayout;
    EditText editMemberEmail;

    //Firebase
    FirebaseAuth mAuth;
    CollectionReference membersCollection;
    CollectionReference usersCollection;
    CollectionReference classesCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);

        initViews();
        initProgressBar();
        initToolbar();
        initFirebase();

    }
    private void initViews(){
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        editMemberEmail = findViewById(R.id.edit_member_email);
    }

    private void initProgressBar(){
        Circle circle = new Circle();
        progress.setIndeterminateDrawable(circle);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Member");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();

        usersCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users));

        membersCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.collection_classes))
                .document(ClassDetailsActivity.classObj.getUid())
                .collection(getString(R.string.collection_members));
    }

    public void addMember(View view) {
        if(TextUtils.isEmpty(editMemberEmail.getText().toString())){
            editMemberEmail.setError(getString(R.string.error_required));
        }else{
            progressBarLayout.setVisibility(View.VISIBLE);
            findMember();
        }
    }

    private void findMember(){

        Query query = usersCollection.whereEqualTo("email", editMemberEmail.getText().toString());

        query.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(queryDocumentSnapshots.getDocuments().isEmpty()){
                    progressBarLayout.setVisibility(View.GONE);
                    Toast.makeText(AddMemberActivity.this, "No members found with that email!", Toast.LENGTH_SHORT).show();
                }else{
                    for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                        addMemberToClassOnFirestore(document.getId());
                    }
                }
            }
        });


    }

    private void addMemberToClassOnFirestore(final String memberUid){
        Member member = new Member(memberUid, editMemberEmail.getText().toString());
        membersCollection.add(member).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if(task.isSuccessful()){
                    addClassToMemberOnFirestore(memberUid);
                }else{
                    progressBarLayout.setVisibility(View.GONE);
                    Toast.makeText(AddMemberActivity.this, "Failed to add member", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    private void addClassToMemberOnFirestore(String memberUid) {

        classesCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(memberUid)
                .collection(getString(R.string.collection_classes));

        classesCollection.document(ClassDetailsActivity.classObj.getUid()).set(ClassDetailsActivity.classObj).
                addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(AddMemberActivity.this, "Member Added", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(AddMemberActivity.this, "Failed to add member", Toast.LENGTH_SHORT).show();
                        }
                        progressBarLayout.setVisibility(View.GONE);
                        finish();
                    }
                });
    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Loading please wait...", Toast.LENGTH_SHORT).show();
    }

    //-------Override Functions------//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            default: //No Action
        }

        return super.onOptionsItemSelected(item);
    }
}
