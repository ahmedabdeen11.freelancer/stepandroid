package com.dell.step.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.adapters.ViewPagerAdapter;
import com.dell.step.models.Class;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import javax.annotation.Nullable;

public class ClassDetailsActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private ViewPager mViewPager;

    private ViewPagerAdapter viewPagerAdapter;

    private TabLayout mTabLayout;

    public static Class classObj;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_details);

        getClassData();
        initFirebase();
        initToolbar();
        initViewPager();
    }

    private void getClassData(){
        classObj = (Class) getIntent().getSerializableExtra("class");
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
    }

    private void initToolbar(){
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(classObj.getName());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViewPager(){
        mViewPager = findViewById(R.id.tabPager);
        mTabLayout = findViewById(R.id.main_tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(viewPagerAdapter);
    }

    public void deleteMember(final String memberUid){

        final CollectionReference currentUserClassesCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.collection_classes));

        CollectionReference memberClassesCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(memberUid)
                .collection(getString(R.string.collection_classes));

        //Remove Member from the class
        Query query = currentUserClassesCollection.document(classObj.getUid())
                .collection(getString(R.string.collection_members))
                .whereEqualTo("uid", memberUid);


        query.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(queryDocumentSnapshots.getDocuments().isEmpty()){
                    //Not found
                }else{

                    //Remove member
                    for (DocumentSnapshot document : queryDocumentSnapshots.getDocuments()) {
                        currentUserClassesCollection.document(classObj.getUid())
                                .collection(getString(R.string.collection_members))
                                .document(document.getId())
                                .delete();
                    }
                }
            }
        });


        //Remove class data from member
        memberClassesCollection.document(classObj.getUid()).delete();

        //Refresh Adapter
        viewPagerAdapter.getMembersFragment().getMembers();

        Toast.makeText(this, "Member Deleted...", Toast.LENGTH_SHORT).show();
    }

    public void deleteQuestion(final String questionUid){

        final CollectionReference questionCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.collection_classes))
                .document(classObj.getUid())
                .collection(getString(R.string.collection_questions));

        questionCollection
                .document(questionUid)
                .delete();

        //Refresh Adapter
        viewPagerAdapter.getQuestionsFragment().getQuestions();

        Toast.makeText(this, "Question Deleted...", Toast.LENGTH_SHORT).show();
    }

    //-----Override Methods-------//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
