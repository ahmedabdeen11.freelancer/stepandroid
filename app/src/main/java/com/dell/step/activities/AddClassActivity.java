package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.models.Class;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class AddClassActivity extends AppCompatActivity {

    //Views
    Toolbar toolbar;
    SpinKitView progress;
    FrameLayout progressBarLayout;
    AppCompatSpinner classTypeSpinner;
    EditText editClassName;


    //Adapter
    private ArrayAdapter<String> adapter;

    //Firebase
    FirebaseAuth mAuth;
    CollectionReference classesCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);

        initViews();
        initToolbar();
        setupTypeSpinner();
        initFirebase();
    }

    private void initViews(){
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        classTypeSpinner = findViewById(R.id.spinner_class_type);
        editClassName = findViewById(R.id.edit_class_name);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Class");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        classesCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.collection_classes));
    }

    public void addClass(View view) {
        if(TextUtils.isEmpty(editClassName.getText().toString())){
            editClassName.setError(getString(R.string.error_required));
        }else{
            addClassToFirestore();
        }
    }

    private void addClassToFirestore(){
        String[] classTypesArr = getResources().getStringArray(R.array.class_session);
        Class classObj = new Class(editClassName.getText().toString(),
                    classTypesArr[classTypeSpinner.getSelectedItemPosition()],
                    mAuth.getCurrentUser().getUid());
        classesCollection.add(classObj).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if(task.isSuccessful()){
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK,returnIntent);
                    Toast.makeText(AddClassActivity.this, "Class Added", Toast.LENGTH_SHORT).show();
                }else{
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_CANCELED, returnIntent);
                    Toast.makeText(AddClassActivity.this, "Failed to add class", Toast.LENGTH_SHORT).show();
                }

                finish();
            }
        });
    }

    private void setupTypeSpinner(){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this , R.array.class_session , R.layout.layout_spinner);
        adapter.setDropDownViewResource(R.layout.layout_spinner);
        classTypeSpinner.setAdapter(adapter);
    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Loading please wait...", Toast.LENGTH_SHORT).show();
    }

    //-------Override Functions------//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
                break;

            default: //No Action
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}
