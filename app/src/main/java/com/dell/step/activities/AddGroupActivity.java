package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.models.Class;
import com.dell.step.models.Group;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

public class AddGroupActivity extends AppCompatActivity {

    //Views
    Toolbar toolbar;
    SpinKitView progress;
    FrameLayout progressBarLayout;
    EditText editGroupName;

    //Firebase
    FirebaseAuth mAuth;
    CollectionReference groupsCollection;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_group);

        initViews();
        initToolbar();
        initFirebase();
    }

    private void initViews(){
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        editGroupName = findViewById(R.id.edit_class_name);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Group");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        groupsCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.collection_groups));
    }

    public void addGroup(View view) {
        if(TextUtils.isEmpty(editGroupName.getText().toString())){
            editGroupName.setError(getString(R.string.error_required));
        }else{
            addClassToFirestore();
        }
    }

    private void addClassToFirestore(){
        Group group = new Group(editGroupName.getText().toString(),
                mAuth.getCurrentUser().getUid());
        groupsCollection.add(group).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if(task.isSuccessful()){
                    Toast.makeText(AddGroupActivity.this, "Group Added", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddGroupActivity.this, "Failed to add group", Toast.LENGTH_SHORT).show();
                }

                finish();
            }
        });
    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Loading please wait...", Toast.LENGTH_SHORT).show();
    }

    //-------Override Functions------//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            default: //No Action
        }

        return super.onOptionsItemSelected(item);
    }
}
