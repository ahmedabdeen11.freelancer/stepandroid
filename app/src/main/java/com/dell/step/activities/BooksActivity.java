package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.adapters.ResourcesAdapter;
import com.dell.step.models.Resource;
import com.github.ybq.android.spinkit.SpinKitView;
import com.github.ybq.android.spinkit.style.Circle;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

public class BooksActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private RecyclerView booksListView;
    private List<Resource> bookList = new ArrayList<>();

    private CollectionReference booksCollection;
    private ResourcesAdapter adapter;
    private FloatingActionButton addNewBookBtn;

    private final int PERMISSIONS_REQUEST_STORAGE = 1;
    private final int REQUEST_PICK_FILE = 1;
    private Uri fileUri;

    private Toolbar toolbar;

    SpinKitView progress;
    FrameLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        initViews();
        initProgressBar();
        initToolbar();
        initFirebase();
        initRecyclerview();
        setOnClickListeners();
        getBooks();
    }

    private void initViews(){
        toolbar = findViewById(R.id.toolbar);
        addNewBookBtn = findViewById(R.id.fab_add_book);
        booksListView = findViewById(R.id.book_list_view);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);

        if(MainActivity.currentUser.getType().equals("Student")){
            addNewBookBtn.setVisibility(View.GONE);
        }
    }

    private void initProgressBar(){
        Circle circle = new Circle();
        progress.setIndeterminateDrawable(circle);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Books");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        booksCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(ClassDetailsActivity.classObj.getInstructorUid())
                .collection(getString(R.string.collection_classes))
                .document(ClassDetailsActivity.classObj.getUid())
                .collection(getString(R.string.collection_books));
    }

    private void setOnClickListeners(){
        addNewBookBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickFile();
            }
        });
    }

    private void pickFile(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}
                    , PERMISSIONS_REQUEST_STORAGE);

        }
        else{
            pickFileFromStorage();
        }
    }

    private void initRecyclerview(){
        adapter = new ResourcesAdapter(this, bookList);
        booksListView.setLayoutManager(new LinearLayoutManager(this));
        booksListView.setAdapter(adapter);
    }

    private void getBooks(){
        booksCollection.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                bookList.clear();
                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    Resource resource = doc.getDocument().toObject(Resource.class);
                    bookList.add(resource);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void pickFileFromStorage() {
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select file"), REQUEST_PICK_FILE);
    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Uploading please wait...", Toast.LENGTH_SHORT).show();
    }

    private void uploadFileToFirebase(){
        final StorageReference ref = FirebaseStorage.getInstance().getReference()
                .child(getString(R.string.storage_books))
                .child(UUID.randomUUID().toString());

        final String fileName = getFileName(fileUri);

        UploadTask uploadTask = ref.putFile(fileUri);

        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    String downloadURL = downloadUri.toString();
                    saveBook(fileName, downloadURL);
                }
                else {
                    progressBarLayout.setVisibility(View.GONE);
                    Toast.makeText(BooksActivity.this, getString(R.string.error_unknown), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveBook(String title, String downloadURL) {
        Resource resource = new Resource(mAuth.getCurrentUser().getUid(), title, downloadURL);
        booksCollection.add(resource).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if(task.isSuccessful()){
                    progressBarLayout.setVisibility(View.GONE);
                    getBooks();
                    Toast.makeText(BooksActivity.this, "File uploaded", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(BooksActivity.this, "Failed to upload", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }

        return result;
    }

    //-----Override Methods------//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            default: //No Action
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_PICK_FILE: {
                    fileUri = data.getData();
                    progressBarLayout.setVisibility(View.VISIBLE);
                    uploadFileToFirebase();
                }
                break;

                default:  //Do nothing
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case PERMISSIONS_REQUEST_STORAGE:

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, getString(R.string.error_permission_denied), Toast.LENGTH_SHORT).show();
                }
                else{
                    pickFileFromStorage();
                }
                break;
        }
    }
}
