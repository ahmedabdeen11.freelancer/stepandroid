package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.dell.step.R;
import com.dell.step.adapters.QuestionsAdapter;
import com.dell.step.adapters.ReplyAdapter;
import com.dell.step.app.Helper;
import com.dell.step.models.Class;
import com.dell.step.models.Question;
import com.dell.step.models.Reply;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.rpc.Help;

import java.util.ArrayList;
import java.util.List;

public class QuestionDetailsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Question question;

    private TextView tvQuestion;
    private TextView tvUsername;

    private FirebaseAuth mAuth;
    private CollectionReference repliesCollection;
    private EditText editReply;

    private List<Reply> replyList = new ArrayList<>();
    private ReplyAdapter adapter;
    private RecyclerView rvReply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_details);

        initToolbar();
        setQuestionData();
        initFirebase();
        setQuestionData();
        initRecyclerview();
        getQuestionReplies();
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        repliesCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(question.getQuestionOwnerUid())
                .collection(getString(R.string.collection_classes))
                .document(ClassDetailsActivity.classObj.getUid())
                .collection(getString(R.string.collection_questions))
                .document(question.getUid())
                .collection(getString(R.string.collection_replies));
    }

    private void initToolbar(){
        toolbar = findViewById(R.id.toolbar);
        editReply = findViewById(R.id.editReply);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Question");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerview(){
        rvReply = findViewById(R.id.rvReplies);
        adapter = new ReplyAdapter(this, replyList);
        rvReply.setLayoutManager(new LinearLayoutManager(this));
        rvReply.setAdapter(adapter);
    }

    private void setQuestionData(){
        question = (Question) getIntent().getSerializableExtra("question");

        tvQuestion = findViewById(R.id.tv_question_txt);
        tvUsername = findViewById(R.id.tv_username);

        tvUsername.setText(question.getUserName());
        tvQuestion.setText(question.getQuestion());
    }

    private void getQuestionReplies(){
        Task<QuerySnapshot> querySnapshotTask = repliesCollection.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                replyList.clear();
                for (DocumentChange doc : task.getResult().getDocumentChanges()) {
                    Reply reply = doc.getDocument().toObject(Reply.class);
                    reply.setUid(doc.getDocument().getId());
                    replyList.add(reply);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void addReplay(View view) {
        if(TextUtils.isEmpty(editReply.getText().toString())){
            editReply.setError("Required");
        }else{
            Helper.hideKeyboard(QuestionDetailsActivity.this);
            Reply reply = new Reply(mAuth.getCurrentUser().getUid(),
                    MainActivity.currentUser.getUsername(), editReply.getText().toString());

            repliesCollection.add(reply);
            editReply.setText("");
            getQuestionReplies();
        }

    }

    //----- Override Methods-------//
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            default: //No Action
        }

        return super.onOptionsItemSelected(item);
    }

}
