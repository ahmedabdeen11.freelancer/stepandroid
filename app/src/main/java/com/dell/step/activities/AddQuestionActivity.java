package com.dell.step.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.models.Question;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.model.value.ServerTimestampValue;

public class AddQuestionActivity extends AppCompatActivity {

    //Views
    Toolbar toolbar;
    SpinKitView progress;
    FrameLayout progressBarLayout;
    EditText editQuestionTxt;

    //Firebase
    FirebaseAuth mAuth;
    CollectionReference questionCollection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_question);

        initViews();
        initToolbar();
        initFirebase();
    }

    private void initViews(){
        toolbar = findViewById(R.id.toolbar);
        progress = findViewById(R.id.progress);
        progressBarLayout = findViewById(R.id.progressBarLayout);
        editQuestionTxt = findViewById(R.id.edit_question_txt);
    }

    private void initToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Question");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        questionCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(ClassDetailsActivity.classObj.getInstructorUid())
                .collection(getString(R.string.collection_classes))
                .document(ClassDetailsActivity.classObj.getUid())
                .collection(getString(R.string.collection_questions));
    }

    public void addQuestion(View view) {
        if(TextUtils.isEmpty(editQuestionTxt.getText().toString())){
            editQuestionTxt.setError(getString(R.string.error_required));
        }else{
            addQuestionToFirestore();
        }
    }

    private void addQuestionToFirestore(){
        Question question = new Question(mAuth.getCurrentUser().getUid(),
                MainActivity.currentUser.getUsername(),
                editQuestionTxt.getText().toString()
                );
        questionCollection.add(question).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                if(task.isSuccessful()){
                    Toast.makeText(AddQuestionActivity.this, "Question Added", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(AddQuestionActivity.this, "Failed to add question", Toast.LENGTH_SHORT).show();
                }

                finish();
            }
        });
    }

    public void progressBarLayout(View view) {
        Toast.makeText(this, "Loading please wait...", Toast.LENGTH_SHORT).show();
    }

    //-------Override Functions------//

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

            default: //No Action
        }

        return super.onOptionsItemSelected(item);
    }
}
