package com.dell.step.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dell.step.R;
import com.dell.step.activities.AddQuestionActivity;
import com.dell.step.activities.ClassDetailsActivity;
import com.dell.step.activities.MainActivity;
import com.dell.step.adapters.QuestionsAdapter;
import com.dell.step.models.Member;
import com.dell.step.models.Question;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class QuestionsFragment extends Fragment {


    View view;
    private FirebaseAuth mAuth;
    private RecyclerView questionListView;
    private List<Question> questionList = new ArrayList<>();

    private QuestionsAdapter adapter;
    private CollectionReference questionCollection;
    private FloatingActionButton addNewQuestionBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_questions, container, false);

        initViews();
        initFirebase();
        setOnClickListeners();
        initFirebase();
        initRecyclerview();

        return view;
    }

    private void initViews(){
        addNewQuestionBtn = view.findViewById(R.id.fab_add_question);
        questionListView = view.findViewById(R.id.questions_list_view);

        if(MainActivity.currentUser.getType().equals("Student")){
            addNewQuestionBtn.setVisibility(View.GONE);
        }
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        questionCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(ClassDetailsActivity.classObj.getInstructorUid())
                .collection(getString(R.string.classes))
                .document(ClassDetailsActivity.classObj.getUid())
                .collection(getString(R.string.collection_questions));
    }

    private void setOnClickListeners(){
        addNewQuestionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddQuestionActivity.class));
            }
        });
    }

    private void initRecyclerview(){
        adapter = new QuestionsAdapter(getActivity(), questionList);
        questionListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        questionListView.setAdapter(adapter);
    }

    public void getQuestions(){

        Task<QuerySnapshot> querySnapshotTask = questionCollection.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                questionList.clear();
                for (DocumentChange doc : task.getResult().getDocumentChanges()) {
                    Question question = doc.getDocument().toObject(Question.class);
                    question.setUid(doc.getDocument().getId());
                    questionList.add(question);
                }
                adapter.notifyDataSetChanged();
            }
        });

        /*questionCollection.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(queryDocumentSnapshots != null){
                    questionList.clear();
                    for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                        Question question = doc.getDocument().toObject(Question.class);
                        question.setUid(doc.getDocument().getId());
                        questionList.add(question);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        getQuestions();
    }
}
