package com.dell.step.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dell.step.R;
import com.dell.step.activities.AddClassActivity;
import com.dell.step.activities.AddGroupActivity;
import com.dell.step.adapters.ClassesAdapter;
import com.dell.step.adapters.GroupsAdapter;
import com.dell.step.models.Class;
import com.dell.step.models.Group;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class GroupsFragment extends Fragment {

    View view;
    private FirebaseAuth mAuth;
    private RecyclerView groupListView;
    private List<Group> groupList = new ArrayList<>();

    private CollectionReference groupsCollection;
    private GroupsAdapter adapter;
    private FloatingActionButton addNewGroupBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_groups, container, false);

        initViews();
        initFirebase();
        setOnClickListeners();
        initRecyclerview();
        getGroups();

        return view.getRootView();
    }

    private void initViews(){
        addNewGroupBtn = view.findViewById(R.id.fab_add_group);
        groupListView = view.findViewById(R.id.groups_list_view);
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        groupsCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.collection_groups));
    }

    private void setOnClickListeners(){
        addNewGroupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddGroupActivity.class));
            }
        });
    }

    private void initRecyclerview(){
        adapter = new GroupsAdapter(groupList);
        groupListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        groupListView.setAdapter(adapter);
    }

    private void getGroups(){
        groupsCollection.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    Group group = doc.getDocument().toObject(Group.class);
                    groupList.add(group);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }



}
