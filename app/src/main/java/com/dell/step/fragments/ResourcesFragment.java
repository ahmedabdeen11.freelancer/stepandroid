package com.dell.step.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dell.step.R;
import com.dell.step.activities.BooksActivity;
import com.dell.step.activities.LecturesActivity;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResourcesFragment extends Fragment {

    private Button lecturesBtn;
    private Button booksBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_resources, container, false);

        lecturesBtn = view.findViewById(R.id.btn_lectures);
        booksBtn = view.findViewById(R.id.btn_books);

        lecturesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), LecturesActivity.class));
            }
        });

        booksBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), BooksActivity.class));
            }
        });

        return view.getRootView();
    }

}


