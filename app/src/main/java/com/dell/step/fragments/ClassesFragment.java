package com.dell.step.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dell.step.R;
import com.dell.step.activities.AddClassActivity;
import com.dell.step.activities.MainActivity;
import com.dell.step.adapters.ClassesAdapter;
import com.dell.step.models.Class;
import com.dell.step.models.Question;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ClassesFragment extends Fragment {

    View view;
    private FirebaseAuth mAuth;
    private RecyclerView classListView;
    private List<Class> classList = new ArrayList<>();

    private ClassesAdapter adapter;
    private CollectionReference classesCollection;
    private FloatingActionButton addNewClassBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_classes, container, false);

        initViews();
        initFirebase();
        setOnClickListeners();
        initFirebase();
        initRecyclerview();
        getClasses();


        return view.getRootView();
    }

    private void initViews(){
        addNewClassBtn = view.findViewById(R.id.fab_add_class);
        classListView = view.findViewById(R.id.class_list_view);

        if(MainActivity.currentUser.getType().equals("Student")){
            addNewClassBtn.setVisibility(View.GONE);
        }
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        classesCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(mAuth.getCurrentUser().getUid())
                .collection(getString(R.string.classes));
    }

    private void setOnClickListeners(){
        addNewClassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getActivity(), AddClassActivity.class), 1);
            }
        });
    }

    private void initRecyclerview(){
        adapter = new ClassesAdapter(getActivity(), classList);
        classListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        classListView.setAdapter(adapter);
    }

    public void getClasses(){
        /*classesCollection.addSnapshotListener(getActivity(), new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {

                for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()) {
                    Class classObj = doc.getDocument().toObject(Class.class);
                    classObj.setUid(doc.getDocument().getId());
                    classList.add(classObj);
                }
                adapter.notifyDataSetChanged();
            }
        });*/

        Task<QuerySnapshot> querySnapshotTask = classesCollection.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                classList.clear();
                for (DocumentChange doc : task.getResult().getDocumentChanges()) {
                    Class classObj = doc.getDocument().toObject(Class.class);
                    classObj.setUid(doc.getDocument().getId());
                    classList.add(classObj);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

}
