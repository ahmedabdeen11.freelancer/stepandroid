package com.dell.step.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dell.step.R;
import com.dell.step.activities.AddClassActivity;
import com.dell.step.activities.AddMemberActivity;
import com.dell.step.activities.ClassDetailsActivity;
import com.dell.step.activities.MainActivity;
import com.dell.step.adapters.ClassesAdapter;
import com.dell.step.adapters.MembersAdapter;
import com.dell.step.models.Class;
import com.dell.step.models.Member;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MembersFragment extends Fragment {

    View view;
    private FirebaseAuth mAuth;
    private RecyclerView memberListView;
    private List<Member> memberList = new ArrayList<>();

    private MembersAdapter adapter;
    private CollectionReference memberCollection;
    private FloatingActionButton addNewMemberBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_members, container, false);

        initViews();
        initFirebase();
        setOnClickListeners();
        initFirebase();
        initRecyclerview();

        return view;
    }

    private void initViews(){
        addNewMemberBtn = view.findViewById(R.id.fab_add_member);
        memberListView = view.findViewById(R.id.members_list_view);

        if(MainActivity.currentUser.getType().equals("Student")){
            addNewMemberBtn.setVisibility(View.GONE);
        }
    }

    private void initFirebase(){
        mAuth = FirebaseAuth.getInstance();
        memberCollection = FirebaseFirestore.getInstance()
                .collection(getString(R.string.collection_users))
                .document(ClassDetailsActivity.classObj.getInstructorUid())
                .collection(getString(R.string.classes))
                .document(ClassDetailsActivity.classObj.getUid())
                .collection(getString(R.string.collection_members));
    }

    private void setOnClickListeners(){
        addNewMemberBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddMemberActivity.class));
            }
        });
    }

    private void initRecyclerview(){
        adapter = new MembersAdapter(getActivity(), memberList);
        memberListView.setLayoutManager(new LinearLayoutManager(getActivity()));
        memberListView.setAdapter(adapter);
    }

    public void getMembers(){

        Task<QuerySnapshot> querySnapshotTask = memberCollection.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                memberList.clear();
                for (DocumentChange doc : task.getResult().getDocumentChanges()) {
                    Member member = doc.getDocument().toObject(Member.class);
//                    member.setUid(doc.getDocument().getId());
                    memberList.add(member);
                }
                adapter.notifyDataSetChanged();
            }
        });

        /*memberCollection.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if (queryDocumentSnapshots != null){

                }
            }
        });*/
    }

    @Override
    public void onResume() {
        super.onResume();
        getMembers();
    }
}
